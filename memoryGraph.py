#An example of 'free output'
#              total        used        free      shared  buff/cache   available
#Mem:        2937508     1778720      527100      424144      631688      581512
#Swap:       2097148      322816     1774332

import os
import time
import subprocess

file_start_time = time.time()
fileName = "./memoryGraph.csv"
write_header = False if os.path.exists(fileName) else True
with open(fileName,"a") as outputFile:
     if write_header:
        outputFile.write("time,mem_total,mem_used,mem_free,mem_shared,mem_buff_cache,mem_available,swap_total,swap_used,swap_free\n")
     while True:
        cmd = "free"
        result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        lines = [str1.decode('utf-8').strip().split()[1:] for str1 in result.stdout.readlines()]
        assert(len(lines)==3)
        str1 = ",".join([str(time.time())] + lines[1]+ lines[2])
        print(str1)
        outputFile.write(str1+"\n")
        time.sleep(1)

